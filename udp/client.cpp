#include <boost/asio.hpp>
#include <iostream>

using namespace boost;
using namespace asio::ip;
using namespace std;

const int PORT = 8001;
const int BUFFER_SIZE = 1024;

const string ADDRESS = "127.0.0.1";
const string LOG_NAME = "[Client] ";
const string MESSAGE = "Актуальная картина дня на RT: \
круглосуточное ежедневное обновление новостей политики, \
бизнеса, финансов, спорта, науки, культуры.";

int main() {
	asio::io_service _service;

	udp::endpoint _endpoint(address::from_string(ADDRESS), PORT);
	udp::socket _socket(_service, udp::endpoint(udp::v4(), 0));

	_socket.send_to(asio::buffer(MESSAGE), _endpoint);

	char buffer[BUFFER_SIZE];

	try {
		size_t bytes_read = _socket.receive_from(asio::buffer(buffer), _endpoint);
		string response(buffer, buffer + bytes_read);
		cout << LOG_NAME << "Got message: \"" << response << "\"" << endl;
	} catch (system::system_error& error) {
		cerr << LOG_NAME << error.what() << endl;
		_socket.close();
		return 1;
	}

	_socket.close();
}
