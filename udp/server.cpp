#include <boost/asio.hpp>
#include <iostream>

using namespace boost;
using namespace asio::ip;
using namespace std;

const int PORT = 8001;
const int BUFFER_SIZE = 1024;
const string LOG_NAME = "[Server] ";

int main() {
	asio::io_service _service;
	udp::endpoint _endpoint;
	udp::socket _socket(_service, udp::endpoint(udp::v4(), PORT));

	char buffer[BUFFER_SIZE];
	while(true) {
		auto bytes = _socket.receive_from(asio::buffer(buffer), _endpoint);
		string message(buffer, bytes);
		cout << LOG_NAME << "Got message: \"" << message << "\"" << endl;
		_socket.send_to(asio::buffer("Echo " + message), _endpoint);
	}
}

