#include <boost/asio.hpp>
#include <iostream>

using namespace boost;
using namespace asio::ip;
using namespace std;

const int PORT = 8001;
const int BUFFER_SIZE = 1024;

const string ADDRESS = "127.0.0.1";
const string LOG_NAME = "[Client] ";
const string MESSAGE = "Актуальная картина дня на RT: \
круглосуточное ежедневное обновление новостей политики, \
бизнеса, финансов, спорта, науки, культуры.";

int main() {
	asio::io_service _service;
	tcp::socket _socket(_service);
	tcp::endpoint _endpoint(address::from_string(ADDRESS), PORT);

	try {
		_socket.connect(_endpoint);
	} catch (system::system_error& error) {
		cerr << LOG_NAME << error.what() << endl;
		return 1;
	}

	_socket.write_some(asio::buffer(MESSAGE));

	char buffer[BUFFER_SIZE];

	size_t bytes_read = _socket.read_some(asio::buffer(buffer));
	string response(buffer, buffer + bytes_read);
	cout << LOG_NAME << "Got message: \"" << response << "\"" << endl;

	_socket.close();
}
